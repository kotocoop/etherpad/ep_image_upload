'use strict';

/**
 * Server-side hooks
 *
 * @see {@link http://etherpad.org/doc/v1.5.7/#index_server_side_hooks}
 */

const eejs = require('ep_etherpad-lite/node/eejs/');
const settings = require('ep_etherpad-lite/node/utils/Settings');
const uuid = require('uuid');
const path = require('path');
const {formidable} = require('formidable');
// const Transform = require('stream').Transform;
const mimetypes = require('mime-db');
const url = require('url');
const fs = require('fs');
// const { Upload } = require("@aws-sdk/lib-storage");
// const { S3Client } = require("@aws-sdk/client-s3");

/**
 * ClientVars hook
 *
 * Exposes plugin settings from settings.json to client code inside clientVars variable
 * to be accessed from client side hooks
 *
 * @param {string} hookName Hook name ("clientVars").
 * @param {object} args Object containing the arguments passed to hook. {pad: {object}}
 * @param {function} cb Callback
 *
 * @returns {*} callback
 *
 * @see {@link http://etherpad.org/doc/v1.5.7/#index_clientvars}
 */
exports.clientVars = (hookName, args, cb) => {
  const pluginSettings = {
    storageType: 'base64',
  };
  if (!settings.ep_image_upload) {
    settings.ep_image_upload = {};
  }
  const keys = Object.keys(settings.ep_image_upload);
  keys.forEach((key) => {
    if (key !== 'storage') {
      pluginSettings[key] = settings.ep_image_upload[key];
    }
  });
  if (settings.ep_image_upload.storage && settings.ep_image_upload.storage.type !== 'base64') {
    pluginSettings.storageType = settings.ep_image_upload.storage.type;
  }

  if (!pluginSettings) {
    console.warn(hookName,
        'ep_image_upload settings not found. The settings can be specified in EP settings.json.',
    );

    return cb();
  }
  pluginSettings.mimeTypes = mimetypes;

  return cb({ep_image_upload: pluginSettings});
};

exports.eejsBlock_body = (hookName, args, cb) => {
  const modal = eejs.require('ep_kotocoop_image_upload/templates/modal.ejs');
  args.content += modal;

  return cb();
};

exports.expressConfigure = (hookName, context) => {
  context.app.post('/p/:padId/pluginfw/ep_kotocoop_image_upload/upload', (req, res, next) => {
    const padId = req.params.padId;

    const storageConfig = settings.ep_image_upload.storage;
    if (storageConfig) {
      const newFileName = uuid.v4();
      let accessPath = '';

      const form = formidable({});

      if (settings.ep_image_upload.storage && settings.ep_image_upload.storage.type === 'local') {
        form.parse(req, (err, fields, files) => {
          const file = files.file[0];

          let savedFilename = path.join(padId, newFileName + path.extname(file.originalFilename));

          let baseURL = settings.ep_image_upload.storage.baseURL;
          if (baseURL.charAt(baseURL.length - 1) !== '/') {
            baseURL += '/';
          }
          accessPath = new url.URL(savedFilename, settings.ep_image_upload.storage.baseURL);
          savedFilename = path.join(settings.ep_image_upload.storage.baseFolder, savedFilename);
          const dirname = path.dirname(savedFilename);
          fs.mkdir(dirname, {recursive: true}, (err) => {
            if (err) throw err;
          });

          fs.createReadStream(file.filepath).pipe(fs.createWriteStream(savedFilename));

          res.status(201).json(accessPath);
        });
      } else {
        res.error('Only local storage supported');

        form.parse(req, (err, fields, files) => {});


        form.on('error', (error) => {
          res.error(error.message);
        });

        form.on('data', (data) => {
          if (data.name === 'successUpload') {
            res.status(201).json(data.value);
          }
        });

        form.on('fileBegin', (formName, file) => {
          /*
          file.open = async function () {
            this._writeStream = new Transform({
              transform(chunk, encoding, callback) {
                callback(null, chunk);
              },
            });

            this._writeStream.on('error', (e) => {
              form.emit('error', e);
            });

            // upload to S3
            new Upload({
              client: new S3Client({
                credentials: {
                  accessKeyId,
                  secretAccessKey,
                },
                region,
              }),
              params: {
                ACL: 'public-read',
                Bucket,
                Key: `${Date.now().toString()}-${this.originalFilename}`,
                Body: this._writeStream,
              },
              tags: [], // optional tags
              queueSize: 4, // optional concurrency configuration
              partSize: 1024 * 1024 * 5, // optional size of each part, in bytes, at least 5MB
              leavePartsOnError: false, // optional manually handle dropped parts
            })
                .done()
                .then((data) => {
                  form.emit('data', {name: 'complete', value: data});
                }).catch((err) => {
                  form.emit('error', err);
                });
          };
          */
        });
      }
    }
  });
};

exports.padRemove = async (hookName, context) => {
  // If storageType is local, delete the folder for the images
  const {ep_image_upload: {storage: {type, baseFolder} = {}} = {}} = settings;
  if (type === 'local') {
    const dir = path.join(baseFolder, context.padID);
    await fs.promises.rmdir(dir, {recursive: true});
  }
};
